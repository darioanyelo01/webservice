export interface UserProfileResponse {
    id: string;
    names: string;
    surnames: string;
    email: string;
  }
  
  export interface UserProfile {
    documentNumber: string;
    documentType: string;
    email: string;
    names: string;
    surnames: string;
    fullName: string;
    id?: string;
    defaultPagePath?: string;
    categoryPage?: number;
    debtPage?: number;
    userPage?: number;
    clientPage?: number;
    providerPage?: number;
    trackingPage?: number;
  }