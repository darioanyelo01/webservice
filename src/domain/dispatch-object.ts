export interface DispatchObject<T, P = any> {
    type: T;
    payload?: P;
  }
  
  export type createDispatchWithActionMap<ActionMap> = <T extends keyof ActionMap, P extends ActionMap[T]>(
    dispatchObject: DispatchObject<T, P>,
  ) => void;