export enum DefineModule {
  Auth = "/auth",
  BackOffice = "/admin",
}

export enum InsideOfficeModule {
  Init = "init",
  Categories = "categoria",
  Subruta = "new",
  Debts = "debts",
  Users = "users",
  
}

export enum AuthModule {
  Login = "login",
}


export const AuthPath = {
    Login: `${DefineModule.Auth}/${AuthModule.Login}`,
  };

  

  export const BackOfficePath = {
    Init: `${DefineModule.BackOffice}/${InsideOfficeModule.Init}`,
    Debts: `${DefineModule.BackOffice}/${InsideOfficeModule.Debts}`,
    Categories: `${DefineModule.BackOffice}/${InsideOfficeModule.Categories}`,
    Users: `${DefineModule.BackOffice}/${InsideOfficeModule.Users}`,
    SubRuta: `${DefineModule.BackOffice}/${InsideOfficeModule.Categories}/new`
   
  };