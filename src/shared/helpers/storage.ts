export enum StorageKey {
    accessToken = "accessToken",
    user = "user",
    profile = "profile",
    external_dl = "external_dl",
  }
  
  export const load = async <T>(key: StorageKey): Promise<T | null> => {
    const response = await sessionStorage.getItem(key);
    if (response) {
      return JSON.parse(response);
    }
    return null;
  };
  
  export const remove = async (key?: StorageKey): Promise<void> => {
    if (!key) {
      const storagesKeys = Object.keys(StorageKey);
      storagesKeys.map(async (keyItem) => {
        await sessionStorage.removeItem(keyItem);
      });
      return;
    }
    await sessionStorage.removeItem(key!);
  };
  
  export const save = async (key: StorageKey, payload: any): Promise<void> => {
    await sessionStorage.setItem(key, JSON.stringify(payload));
  };
  
  export const Storage = {
    load,
    remove,
    save,
  };
  