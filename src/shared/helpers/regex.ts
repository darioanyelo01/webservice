const isValidEmail = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
const phoneNumber = /^9[\d]{8}/;
const onlyNumber = /^[-+]?(\d+|\d+\.\d*|\d*\.\d+)$/;

export const Regex = {
  isValidEmail,
  phoneNumber,
  onlyNumber,
};
