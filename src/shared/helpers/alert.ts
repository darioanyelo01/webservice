import Swal, { SweetAlertIcon } from "sweetalert2";
import { Colors } from "../constants/colors";

type AlertProperties = {
  title?: string;
  text?: string;
  confirmButtonColor?: string;
  cancelButtonColor?: string;
  type?: SweetAlertIcon;
};

const Alert = {
  error: ({
    title = "Oops",
    text = "Hubo un problema al cargar la información",
    confirmButtonColor = Colors.primary,
    cancelButtonColor = Colors.red,
    type = "error",
  }: AlertProperties = {}) => {
    return Swal.fire({
      title,
      text,
      icon: type,
      confirmButtonText: "Aceptar",
      confirmButtonColor,
      cancelButtonColor,
      allowOutsideClick: false,
    });
  },
  success: ({
    title = "Listo",
    text = "Se registró la información de manera satisfactoria",
    confirmButtonColor = Colors.primary,
    cancelButtonColor = Colors.red,
  }: AlertProperties = {}) => {
    return Swal.fire({
      title,
      text,
      icon: "success",
      confirmButtonText: "Aceptar",
      confirmButtonColor,
      cancelButtonColor,
      allowOutsideClick: false,
    });
  },
  question({
    title = "¿Está seguro que desea continuar?",
    text = "",
    confirmButtonColor = Colors.primary,
    cancelButtonColor = Colors.red,
  }: AlertProperties = {}) {
    return Swal.fire({
      title,
      text,
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Sí, continuar",
      confirmButtonColor,
      cancelButtonColor,
      cancelButtonText: "Cancelar",
      allowOutsideClick: false,
    });
  },
};

export default Alert;
