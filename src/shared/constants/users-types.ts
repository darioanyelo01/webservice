export enum UserType {
    Administrator = 3,
    Maintainer = 4,
  }
  
  export enum ProviderType {
    Private = 1,
    Company = 2,
  }
  
  export const ProviderTypes = [
    { value: ProviderType.Private, title: "Independiente" },
    { value: ProviderType.Company, title: "Empresa" },
  ];
  
  export const UserTypeName = {
    [UserType.Administrator]: "Administrador",
    [UserType.Maintainer]: "Mantenedor",
  };
  
  export const UserTypes = [
    {
      title: UserTypeName[UserType.Administrator],
      value: UserType.Administrator,
    },
    { title: UserTypeName[UserType.Maintainer], value: UserType.Maintainer },
  ];