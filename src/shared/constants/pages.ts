export enum AccessPages {
    NoAccess = 1,
    Read = 2,
    Edit = 3,
  }
  
  export const AccessPagesName = {
    [AccessPages.NoAccess]: "Sin Acceso",
    [AccessPages.Read]: "Lectura",
    [AccessPages.Edit]: "Edición",
  };
  
  export const AccessPagesList = [
    {
      title: AccessPagesName[AccessPages.NoAccess],
      value: AccessPages.NoAccess,
    },
    { title: AccessPagesName[AccessPages.Read], value: AccessPages.Read },
    { title: AccessPagesName[AccessPages.Edit], value: AccessPages.Edit },
  ];
  