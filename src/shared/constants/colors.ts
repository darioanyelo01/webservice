export enum Colors {
    primary = "#00b3d0",
    secondary = "#e5e6f0",
    tertiary = "#ff6b55",
    red = "red",
    green = "green",
    white = "#ffffff",
    purple = "#26264F",
    lightPurple = "#91B3FA",
    gray = "#959494",
    grayLight = "#f7f7f7",
    grayDisabled = "#ffffff66",
    grayLightDark = "#B8B8B8",
  }
  