export const Messages = {
    RecordSaved: "Se guardo correctamente",
    DeletedCorrectly: "Se eliminó correctamente",
    UserDoesntExist: "El correo ingresado no existe",
    DoesntHaveAccess: "Usted no tiene acceso a este sitio",
    GenericError: "Algo salió mal, intentelo en unos momentos.",
    EmailSent: "Se envió un correo con los pasos",
  };
  
  export const FormMessage = {
    required: "Este campo es requerido",
  };
  
  export enum AuthErrorResponse {
    UserDoesntExist = "Firebase: Error (auth/wrong-password).",
  }
  
  export const getAuthErrorMessages = (message: string) => {
    switch (message) {
      case AuthErrorResponse.UserDoesntExist:
        return "El correo o contraseña son incorrectos";
      default:
        return Messages.GenericError;
    }
  };
  
  export const getProviderErrorMessages = (message: string) => {
    if (message.includes("this is email is already in use")) {
      return "El correo ingresado ya está en uso";
    }
  
    if (message.includes("cellphone dup key")) {
      return "El celular ingresado ya está en uso";
    }
  
    return Messages.GenericError;
  };