import LocationCityIcon from "@mui/icons-material/LocationCity";
import RequestQuote from "@mui/icons-material/RequestQuote";
import { Colors } from "./colors";
import { InsideOfficeModule } from "../../modules-routes";

interface MenuProps {
  path: string;
  text: string;
  icon: any;
}

export const getMenu = (): MenuProps[] => {
  const menus: MenuProps[] = [];
  menus.push({
    path: InsideOfficeModule.Categories,
    text: "Categorías",
    icon: <LocationCityIcon sx={{ color: Colors.primary, mr: 2, my: -0.8 }} />,
  });

  menus.push({
    path: InsideOfficeModule.Debts,
    text: "Deudas",
    icon: <RequestQuote sx={{ color: Colors.primary, mr: 2, my: -0.8 }} />,
  });

  return menus;
};
