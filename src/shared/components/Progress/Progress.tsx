import React from "react";
import CircularProgress from "@mui/material/CircularProgress";
import Stack from "@mui/material/Stack";

const Progress: React.FC = () => {
  return (
    <Stack height="100vh" alignItems="center" justifyContent="center">
      <CircularProgress />
    </Stack>
  );
};

export default Progress;