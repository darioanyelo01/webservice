import Logo from "./logo.png";
import NoProfile from "./defaultUserImage.png";
import NoProfileWhite from "./defaultUserImageWhite.png";
import WhiteLogo from "./logo-vive-blanco.png";

export const ViveImage = {
  Logo,
  NoProfile,
  NoProfileWhite,
  WhiteLogo,
};
