import React, { useState } from "react";
import Button from "@mui/material/Button";
import { useNavigate } from "react-router-dom";
import TextField from "@mui/material/TextField";
import HttpsIcon from "@mui/icons-material/Https";
import MailOutlineIcon from "@mui/icons-material/MailOutline";
import "./Login.scss";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import Link from "@mui/material/Link";
import { useForm } from "react-hook-form";
// import { ViveImage } from "shared/images";
// import useDispatchState from "shared/hooks/useDispatchState";
// import { ViveAppContext } from "context/vive/app-context";
// import { AuthService } from "proxy/Auth.service";
// import { GlobalActions, ViveAppAction } from "context/vive/actions";
// import { getAuthErrorMessages, Messages } from "shared/constants/Messages";
// import { BackOfficePath } from "modules-routes";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import { Regex } from "../../../../shared/helpers/regex";
// import { getAuthErrorMessages, Messages } from "../../../../shared/constants/Messages";
import { ViveImage } from "../../../../shared/images";
import { BackOfficePath } from "../../../../modules-routes";
// import useDispatchState from "../../../../shared/hooks/useDispatchState";
// import { AuthService } from "../../../../proxy/Auth.service";
// import { Regex } from "shared/helpers/regex";

enum LoginProps {
  email = "email",
  password = "password",
}

interface LoginForm {
  email: string;
  password: string;
}

const Login: React.FC = () => {
//   const [, dispatch] = useDispatchState(ViveAppContext);
  const navigate = useNavigate();
  const [openModal, setOpenModal] = useState(false);
  const [emailForgotten, setEmailForgotten] = useState("");
  const [emailForgottenError, setEmailForgottenError] = useState({
    message: "",
    valid: true,
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      email: "",
      password: ""
    }
  });

  const login = async ({ email, password }: LoginForm) => {
    // try {
    //   await AuthService.loginWithEmail(dispatch, email, password);
    //   const { hasAccess, profile } = await AuthService.checkAccess(dispatch);

    //   if (hasAccess) {
    //     dispatch({ type: ViveAppAction.SetProfile, payload: profile });
      navigate(BackOfficePath.Init);
    //     return;
    //   }

     //
    //   GlobalActions.showSnackBar({
    //     message: Messages.DoesntHaveAccess,
    //     type: "error",
    //   });
    // } catch (error) {
    //   const message = getAuthErrorMessages((error as any).message);
    //   GlobalActions.showSnackBar({ message, type: "error" });
    // }
  };

  const handleForgotPassword = async () => {
    if (!Regex.isValidEmail.test(emailForgotten)) {
      setEmailForgottenError({
        message: "Correo incorrecto",
        valid: false,
      });
      return;
    }

    setEmailForgottenError({
      message: "",
      valid: true,
    });

    // try {
    //   await AuthService.forgotPassword(dispatch, emailForgotten);
    //   GlobalActions.showSnackBar({ message: Messages.EmailSent });
    // } catch (error) {
    //   GlobalActions.showSnackBar({
    //     message: Messages.GenericError,
    //     type: "error",
    //   });
    // } finally {
    //   setEmailForgotten("");
    //   setOpenModal(false);
    // }
  };

  const showForgotPasswordModal = () => {
    setOpenModal(true);
  };

  const closeForgotPasswordModal = () => {
    setOpenModal(false);
    setEmailForgotten("");
  };

  return (
    <>
      <Box height={"85vh"} display="flex" className="px-4">
        <Grid container justifyContent="center" alignItems="center">
          <Grid item md={5} xs={12} className="bg-white py-2">
            <div className="text-center">
              <img src={ViveImage.Logo} alt="Logo Vive" height={50} />
            </div>
            <Typography
              sx={{ mx: "auto", textAlign: "center", fontSize: "20px" }}
              component="div"
              gutterBottom
            >
              Sistema de Gestión
            </Typography>
            <form className="container__form">
              <div className="container__form__text">
                <Box sx={{ display: "flex", alignItems: "flex-end" }}>
                  <MailOutlineIcon sx={{ mr: 1, my: 0.5 }}></MailOutlineIcon>
                  <TextField
                    fullWidth
                    id="standard-basic"
                    label="Correo"
                    variant="standard"
                    {...register(LoginProps.email, {
                      required: "correo incorrecto",
                      pattern: {
                        value: Regex.isValidEmail,
                        message: "correo incorrecto",
                      },
                    })}
                    error={!!errors?.email}
                    // helperText={errors?.email ? errors.email.message : null}
                  />
                </Box>
              </div>
              <div className="container__form__text">
                <Box sx={{ display: "flex", alignItems: "flex-end" }}>
                  <HttpsIcon sx={{ mr: 1, my: 0.5 }}></HttpsIcon>
                  <TextField
                    fullWidth
                    id="standard-password-input"
                    label="Contraseña"
                    type="password"
                    autoComplete="current-password"
                    variant="standard"
                    {...register(LoginProps.password, {
                      required: "Contraseña Incorrecta",
                    })}
                    error={!!errors?.password}
                    // helperText={
                    //   errors?.password ? errors.password.message : null
                    // }
                  />
                </Box>
              </div>
              <div className="container__form__forgot">
                <Link
                  href="#"
                  underline="always"
                  onClick={showForgotPasswordModal}
                >
                  Olvide mi contraseña
                </Link>
              </div>
              <div className="container__form__button">
                <Button
                  variant="contained"
                  type="submit"
                 onClick={handleSubmit(login)}
                >
                  Iniciar Sesión
                </Button>
              </div>
            </form>
          </Grid>
        </Grid>
      </Box>
      <Dialog open={openModal} onClose={closeForgotPasswordModal}>
        <DialogTitle>Recuperar Contraseña</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Por favor ingresa el correo electronico para recibir los pasos de
            recuperación.
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            label="Correo"
            type="email"
            fullWidth
            variant="standard"
            value={emailForgotten}
            onChange={({ target }) => {
              const email = target.value;
              setEmailForgotten(email);
            }}
            helperText={emailForgottenError?.message}
            error={!emailForgottenError?.valid}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={closeForgotPasswordModal}>Cancelar</Button>
          <Button onClick={handleForgotPassword}>Solicitar</Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default Login;