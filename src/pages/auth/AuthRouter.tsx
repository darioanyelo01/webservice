import React, { lazy, Suspense } from "react";
import { Navigate as Redirect, useNavigate } from "react-router-dom";
import { Routes as Switch, Route } from "react-router-dom";
// import Progress from "shared/components/Progress";
// import { AuthModule, AuthPath, BackOfficePath } from "modules-routes";
// import { AuthService } from "proxy/Auth.service";
// import useDispatchState from "shared/hooks/useDispatchState";
// import { ViveAppContext } from "context/vive/app-context";
// import { ViveAppAction } from "context/vive/actions";
import Progress from "../../shared/components/Progress";
import { AuthModule } from "../../modules-routes";
// import useDispatchState from "../../shared/hooks/useDispatchState";
// import { Storage, StorageKey } from "shared/helpers/storage";
// import { WebSite } from "shared/constants/web-site";

const Login = lazy(() => import("./pages/Login/Login"));

export const AuthRoutes: React.FC = () => {
  // const [{ isFetching }, dispatch] = useDispatchState();
   const navigate = useNavigate();

//   useEffect(() => {
//     (async () => {
//       const { hasAccess, profile } = await AuthService.checkAccess(dispatch);
//       if (hasAccess) {
//         dispatch({ type: ViveAppAction.SetProfile, payload: profile });
//         navigate(BackOfficePath.Categories);
//       } else {
//         await AuthService.logout(dispatch);
//         navigate(AuthPath.Login);
//       }
//     })();
//     // eslint-disable-next-line react-hooks/exhaustive-deps
//   }, []);

//   useEffect(() => {
//     const checkIfIsDL = async () => {
//       dispatch({ type: ViveAppAction.StartFetching });
//       const hasDL = await Storage.load(StorageKey.external_dl);
//       if (Boolean(hasDL)) {
//         await Storage.remove(StorageKey.external_dl);
//         window.location.replace(WebSite.url);
//       } else {
//         await Storage.remove(StorageKey.external_dl);
//       }
//     };

//     checkIfIsDL();
//   }, []);

//   if (isFetching) {
//     return <Progress />;
//   }

  return (
    <Suspense fallback={<Progress />}>
      <Switch>
        <Route path={AuthModule.Login} element={<Login />} />
        <Route path="*" element={<Redirect to={AuthModule.Login} />} />
      </Switch>
    </Suspense>
  );
};

export const AuthRouter: React.FC = () => {
  return <AuthRoutes />;
};
