import Grid from "@mui/material/Grid";
import { useNavigate } from "react-router-dom";
import { toBack } from "../../../../../shared/constants/global";

export const SubRuta = () => {
    const navigate = useNavigate();
    const goBack = () => {
        navigate(toBack);
    }

  return (
    <Grid marginY={12} container>
      <div>ES dentro de una sub ruta</div>
      <button type="button" onClick={goBack}>Ir atrás</button>
    </Grid>
  );
};
