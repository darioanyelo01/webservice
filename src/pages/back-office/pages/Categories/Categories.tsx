import Grid from "@mui/material/Grid";
import React from "react";
import { useNavigate } from "react-router-dom";
import { BackOfficePath } from "../../../../modules-routes";
const SubCategories = () => {
  const navigate = useNavigate();

  const newCategory = () => {
    navigate(BackOfficePath.SubRuta);
  };
  return (
    <Grid container marginY={12} paddingX={3}>
      <div> Este es la SubCategories donde crearemos las JTable</div>
      <button onClick={newCategory} type="button">
        Crear Categoria
      </button>
    </Grid>
  );
};
export default SubCategories;
