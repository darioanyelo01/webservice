/* eslint-disable react-hooks/exhaustive-deps */
import React, { lazy, Suspense } from "react";
import { Routes as Switch, Route, useNavigate } from "react-router-dom";
import { Navigate as Redirect, useLocation } from "react-router";
import "./BackOfficeRouter.scss";
import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import AppBar from "@mui/material/AppBar";
import CssBaseline from "@mui/material/CssBaseline";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import ListItemIcon from "@mui/material/ListItemIcon";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";

import AccountCircleRoundedIcon from "@mui/icons-material/AccountCircleRounded";

import Avatar from "@mui/material/Avatar";
import Hidden from "@mui/material/Hidden";

import { ViveImage } from "../../shared/images";
import { Colors } from "../../shared/constants/colors";

import Alert from "../../shared/helpers/alert";

import { Grid } from "@mui/material";

import { AuthPath, InsideOfficeModule } from "../../modules-routes";

import Progress from "../../shared/components/Progress";
import { getMenu } from "../../shared/constants/menu";

import { makeStyles } from "@mui/styles";
import { Debts } from "./pages/DebtHistory/DebtHistory";
import { DebtDetail } from "./pages/DebtHistory/Detail/DetailHistoryDetail";
import { SubRuta } from "./pages/Categories/SubRuta";

const Categories = lazy(() => import("./pages/Categories/Categories"));

const drawerWidth = 240;
const useStyles = makeStyles((theme: any) => ({
  drawer: {
    [theme.breakpoints.up("sm")]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  menuSection: {
    paddingTop: "calc(100% - 11.2rem)",
    height: "100vh",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
  },
}));

export const BackOfficeRoutes: React.FC = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const classes = useStyles();
  const [mobileOpen, setMobileOpen] = React.useState(false);

  function handleDrawerToggle() {
    setMobileOpen(!mobileOpen);
  }

  //   useEffect(() => {
  //     (async () => {
  //       const { hasAccess, profile } = await AuthService.checkAccess(dispatch);
  //       if (hasAccess) {
  //         const currentLocation = location.pathname;
  //         dispatch({ type: ViveAppAction.SetProfile, payload: profile });
  //         navigate(currentLocation);
  //       } else {
  //         await AuthService.logout(dispatch);
  //         navigate(AuthPath.Login);
  //       }
  //     })();
  //     // eslint-disable-next-line react-hooks/exhaustive-deps
  //   }, []);

  const logout = async () => {
    const response = await Alert.question({
      title: "¿Estas seguro de que quiere cerrar sesión?",
    });
    if (response.isConfirmed) {
      // await AuthService.logout(dispatch);
      navigate(AuthPath.Login);
    }
  };

  const MenuSection = () => {
    return (
      <Box className={classes.menuSection}>
        <Box>
          <List>
            <ListItem>
              <div className="w-100">
                <Avatar
                  alt="profile"
                  src={ViveImage.NoProfileWhite}
                  sx={{ width: 56, height: 56 }}
                  className="m-auto"
                />
                <div style={{ marginTop: "0.5rem" }}>
                  <Typography
                    variant="body2"
                    noWrap
                    component="div"
                    fontWeight={600}
                    sx={{ whiteSpace: "normal" }}
                    textAlign="center"
                  >
                    Bienvenido
                  </Typography>
                  <Typography
                    variant="body2"
                    noWrap
                    component="div"
                    fontWeight={600}
                    sx={{ whiteSpace: "normal" }}
                    textAlign="center"
                  >
                    Administrador
                  </Typography>
                </div>
              </div>
            </ListItem>
            {getMenu().map((item, index) => {
              const { text } = item;
              return (
                <ListItem
                  button
                  key={index}
                  onClick={() => {
                    navigate(item.path);
                    handleDrawerToggle();
                  }}
                >
                  <ListItemIcon>{item.icon}</ListItemIcon>
                  <ListItemText primary={text} />
                </ListItem>
              );
            })}
          </List>
        </Box>
        <Box>
          <List>
            <ListItem button onClick={logout}>
              <ListItemIcon>
                <AccountCircleRoundedIcon
                  sx={{ color: Colors.gray, mr: 2, my: -0.8 }}
                />
              </ListItemIcon>
              <ListItemText primary="Cerrar Sesión" />
            </ListItem>
          </List>
        </Box>
      </Box>
    );
  };

  return (
    <Suspense fallback={<Progress />}>
      <Box sx={{ display: "flex" }}>
        <CssBaseline />
        <AppBar
          position="fixed"
          sx={{ zIndex: (theme) => theme.zIndex.drawer + 1 }}
        >
          <Toolbar >
            <IconButton
              color="inherit"
              aria-label="Open drawer"
              edge="start"
              onClick={handleDrawerToggle}
              className={classes.menuButton}
              sx={{
                display: { xs: "block", sm: "none" },
              }}
            >
              <MenuIcon />
            </IconButton>
            <img
              src={ViveImage.WhiteLogo}
              height="30"
              alt="Logo react"
              className="pe-2"
            />
            <div style={{ marginTop: "0.5rem" }}>
              <Typography
                variant="body2"
                noWrap
                component="div"
                fontWeight={600}
              >
                Sistema de Gestión
              </Typography>
            </div>
          </Toolbar>
        </AppBar>

        <nav className={classes.drawer}>
          <Hidden smUp implementation="css">
            <Drawer
              variant="temporary"
              open={mobileOpen}
              onClose={handleDrawerToggle}
              ModalProps={{
                keepMounted: true,
              }}
              sx={{
                display: { xs: "block", sm: "none" },
                "& .MuiDrawer-paper": {
                  boxSizing: "border-box",
                  width: drawerWidth,
                },
              }}
            >
              <MenuSection />
            </Drawer>
          </Hidden>

          <Hidden xsDown implementation="css">
            <Drawer
              variant="permanent"
              sx={{
                display: { xs: "none", sm: "block" },
                "& .MuiDrawer-paper": {
                  boxSizing: "border-box",
                  width: drawerWidth,
                },
              }}
            >
              <MenuSection />
            </Drawer>
          </Hidden>
        </nav>

        <Grid container className="main-container">
          <Grid item md={12} xs={12}>
            <Switch>
              <Route
                path={InsideOfficeModule.Categories}
                element={<Categories />}
              />

              <Route
                path={`${InsideOfficeModule.Categories}/new`}
                element={<SubRuta/>}
              />

              <Route path={InsideOfficeModule.Debts} element={<Debts />} />
              <Route
                path={`${InsideOfficeModule.Debts}/:debtId`}
                element={<DebtDetail />}
              />

              <Route
                path="*"
                element={<Redirect to={InsideOfficeModule.Categories} />}
              />
            </Switch>
          </Grid>
        </Grid>
      </Box>
    </Suspense>
  );
};

export const BackOfficeRouter: React.FC = () => <BackOfficeRoutes />;
