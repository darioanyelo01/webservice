// import { UserType } from "shared/constants/users-types";

import { UserType } from "../../../shared/constants/users-types";

export interface UserDetailResponse {
  id: string;
  document_number: string;
  surnames: string;
  document_type: number;
  user_type: number;
  names: string;
  email: string;
  category_page: number;
  debt_page: number;
  user_page: number;
  client_page: number;
  provider_page: number;
  tracking_page: number;
}

export interface UserMapper {
  id: string;
  names: string;
  surnames: string;
  userType: UserType;
  documentType: number;
  documentNumber: string;
  email?: string;
  categoryPage?: number;
  debtPage?: number;
  userPage?: number;
  clientPage?: number;
  providerPage?: number;
  trackingPage?: number;
}