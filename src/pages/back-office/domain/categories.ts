export interface CategoryDetailResponse {
    _id: string;
    picture_url: string;
    file_name: string;
    file_path: string;
    title: string;
    additional_sections: string[];
    active: boolean;
    order: number;
  }
  
  export interface CategoryMapper {
    id: string;
    pictureUrl: string;
    title: string;
    additionalSections: string[];
    active?: boolean;
    roomAttribute?: boolean;
    bathroomAttribute?: boolean;
    mts2Attribute?: boolean;
    fileName?: string;
    filePath?: string;
    order?: number;
  }