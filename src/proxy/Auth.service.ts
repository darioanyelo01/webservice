// import { UserProfile } from "domain/user";
// import { HttpMethod } from "shared/helpers/http/customFetch";
// import { ViveAppAction } from "../context/vive/actions";
// import { ViveAppDispatch } from "../context/vive/app-context";
// import { Firebase } from "../shared/config/firebase";
// import { customError, customHTTP } from "../shared/helpers/http";

import { ViveAppAction } from "../context/vive/actions";
import { ViveAppDispatch } from "../context/vive/app-context";

// const loginWithEmail = async (
//   dispatch: ViveAppDispatch,
//   email: string,
//   password: string
// ) => {
//   dispatch({ type: ViveAppAction.StartFetching });
//   try {
//     await Firebase.loginWithEmail(email, password);
//   } catch (error) {
//     throw customError(error);
//   } finally {
//     dispatch({ type: ViveAppAction.FinishedFetching });
//   }
// };

// const forgotPassword = async (dispatch: ViveAppDispatch, email: string) => {
//   dispatch({ type: ViveAppAction.StartFetching });
//   try {
//     await Firebase.forgotPassword(email);
//   } catch (error) {
//     throw customError(error);
//   } finally {
//     dispatch({ type: ViveAppAction.FinishedFetching });
//   }
// };

// const publicRecoveryPassword = async (
//   dispatch: ViveAppDispatch,
//   payload: { token: string; password: string; confirmPassword: string }
// ) => {
//   dispatch({ type: ViveAppAction.StartFetching });
//   try {
//     await customHTTP({
//       endPoint: "/admin/update-public-password",
//       method: HttpMethod.Post,
//       body: {
//         token: payload.token,
//         password: payload.password,
//         confirm_password: payload.confirmPassword,
//       },
//     });
//   } catch (error) {
//     throw customError(error);
//   } finally {
//     dispatch({ type: ViveAppAction.FinishedFetching });
//   }
// };

 const logout = async (dispatch: ViveAppDispatch) => {
    dispatch({ type: ViveAppAction.StartFetching });
//    try {
//      await Firebase.logout();
//    } catch (error) {
//     throw customError(error);
//   } finally {
//     dispatch({ type: ViveAppAction.FinishedFetching });
//   }
  };

// const checkAccess = async (
//   dispatch: ViveAppDispatch
// ): Promise<{
//   hasAccess: boolean;
//   profile: UserProfile | null;
// }> => {
//   dispatch({ type: ViveAppAction.StartFetching });
//   try {
//     return await Firebase.getCurrentSession();
//   } catch (error) {
//     throw customError(error);
//   } finally {
//     dispatch({ type: ViveAppAction.FinishedFetching });
//   }
// };

export const AuthService = {

 logout,

};