
// import { Plan } from "../../domain/plan";
import { UserProfile } from "../../domain/user";
import { ViveAppAction} from "./actions";

export interface ViveAppState {
  isFetching: boolean;
  isLogged: boolean;
  profile: UserProfile | null;
  
}

export const viveAppInitialState: ViveAppState = {
  isFetching: false,
  isLogged: false,
  profile: null,
  
};

// export const viveAppContextReducer = (
//   state = viveAppInitialState,
//   { type, payload }: ViveAppDispatchObject<any>
// ) => {
//   switch (type) {
//     case ViveAppAction.StartFetching:
//       return {
//         ...state,
//         isFetching: true,
//       };
//     case ViveAppAction.FinishedFetching:
//       return {
//         ...state,
//         isFetching: false,
//       };
//     case ViveAppAction.SetProfile:
//       return {
//         ...state,
//         profile: payload,
//       };
//     case ViveAppAction.SetCategories:
//       return {
//         ...state,
//         categories: payload,
//       };
//     case ViveAppAction.SetIsLogged:
//       return {
//         ...state,
//         isLogged: payload,
//       };
//     default:
//       return state;
//   }
// };
