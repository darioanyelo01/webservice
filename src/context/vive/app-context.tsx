import React, {
  createContext,
  PropsWithChildren,
  useMemo,
  useReducer,
} from "react";
import { DispatchState } from "../../shared/hooks/useDispatchState";
import { ViveAppDispatchObject } from "./actions";
import { ViveAppState } from "./reduce";

// export const AppContext = () => {
//   return (
//     <div>app-context</div>
//   )
// }
export type ViveAppDispatch<T = any> = React.Dispatch<ViveAppDispatchObject<T>>;

export type ViveAppContextType = DispatchState<ViveAppState, ViveAppDispatch>;

export const ViveAppContext = createContext({} as ViveAppContextType);

