// import toast from "react-hot-toast";
// import { DispatchObject } from "../../domain/dispatch-object";
// import {
//   SnackBarTime,
//   SnackBarType,
//   ToastPosition,
// } from "../../domain/snackbar";

import { DispatchObject } from "../../domain/dispatch-object";

// export enum ViveAppAction {
//   StartFetching = "START_FETCHING",
//   FinishedFetching = "FINISHED_FETCHING",
//   SetProfile = "SET_PROFILE",
//   SetCategories = "SET_CATEGORIES",
//   SetIsLogged = "SET_IS_LOGGED",
// }

// export type ViveAppDispatchObject<T> = DispatchObject<ViveAppAction, T>;

// const showSnackBar = ({
//   message = "",
//   type = "success",
//   duration = SnackBarTime.Medium,
//   position = "top-center",
// }: {
//   message: string;
//   type?: SnackBarType;
//   duration?: SnackBarTime;
//   position?: ToastPosition;
// }) => {
//   if (type === "success") {
//     return toast.success(message, { duration, position });
//   }
//   if (type === "error") {
//     return toast.error(message, { duration, position });
//   }
//   return toast(message, { duration, position });
// };

export enum ViveAppAction {
    StartFetching = "START_FETCHING",
    FinishedFetching = "FINISHED_FETCHING",
    SetProfile = "SET_PROFILE",
    SetCategories = "SET_CATEGORIES",
    SetIsLogged = "SET_IS_LOGGED",
  }

  export type ViveAppDispatchObject<T> = DispatchObject<ViveAppAction, T>; 

export const GlobalActions = {
  showSnackBar:"",
};