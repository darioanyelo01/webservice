import React from "react";
import ReactDOM from "react-dom/client";

import App from "./App";
import { createTheme, ThemeProvider } from "@mui/material";
import "./index.scss";

const theme = createTheme({
  palette:{
    primary:{
      main:"#FA5733",
    }
  }
});

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <App />
    </ThemeProvider>
  </React.StrictMode>
);
