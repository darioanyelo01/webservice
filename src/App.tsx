import {
  Navigate as Redirect,
  Route,
  Routes as Switch,
} from "react-router-dom";
// import { Navigate as Redirect, useNavigate } from "react-router-dom";
import { AuthPath, DefineModule } from "./modules-routes";
import { BrowserRouter as Router } from "react-router-dom";
import { AuthRouter } from "./pages/auth/AuthRouter";
import { BackOfficeRouter } from "./pages/back-office/BackOfficeRouter";

const App: React.FC = () => {
  //const [{ isFetching }] = useDispatchState(ViveAppContext);

  return (
    <Router>
      <Switch>
        <Route path={`${DefineModule.Auth}/*`} element={<AuthRouter />} />

        <Route
          path={`${DefineModule.BackOffice}/*`}
          element={<BackOfficeRouter />}
        />

        <Route path="*" element={<Redirect to={AuthPath.Login} />} />
      </Switch>
    </Router>
  );
};

export default App;
