import { Backdrop, CircularProgress } from '@mui/material';
import React, { FC } from 'react';

interface GenericSpinnerProps {
  children?: React.ReactNode;
}

const GenericSpinner: FC<GenericSpinnerProps> = ({ children }) => {
  return (
    <>
      <Backdrop sx={{ color: '#fff', zIndex: 1305 }} open={true}>
        <CircularProgress color="inherit" />
      </Backdrop>
      {children}
    </>
  );
};

export default GenericSpinner;